package ocwebserver.model

/**
 * Complexity level that can be used for a course.
 *
 * Possible values:
 * - EASY
 * - MEDIUM
 * - HARD
 */
enum class ComplexityLevel {EASY, MEDIUM, HARD}

/**
 * Open Classrooms course.
 *
 * @property title course name
 * @property content course content
 * @property isEnabled indicates if the course is published (default: true)
 * @property complexityLevel level of complexity of the course (default: MEDIUM)
 * @property stars number of stars (default: 0)
 * @property id course unique identifier (default: last ID plus one)
 * @constructor Creates a course
 * @sample Course("Course title", "Course content", stars=6)
 */
data class Course(var title: String,
                  var content: String,
                  var isEnabled: Boolean = true,
                  var complexityLevel: ComplexityLevel = ComplexityLevel.MEDIUM,
                  var stars: Int = 0,
                  val id: Int = generateId()) {

    companion object {
        var uniqueIdSet = mutableSetOf<Int>(0)
        /**
         * Generate course unique identifier.
         *
         * @return id (last ID plus one)
         */
        fun generateId(): Int {
            val tmp = uniqueIdSet.max()?.plus(1)!!
            uniqueIdSet.add(tmp)
            return tmp
        }
    }
}