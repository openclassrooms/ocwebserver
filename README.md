# OCWebServer

Créer une API web chargée de récupérer des cours au format JSON.  

Pour plus de détail, voici [le lien de l'énoncé](https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin/exercises/2634#/step1).  

## Outils et technologies utilisés

Les outils et technologies utilisés sont liées à l'énoncé mais aussi à mes habitudes de travail :  

  - **IntelliJ IDEA :** Environnement de Développement Intégré (IDE)
  - **Kotlin :** langage de développement
  - **Ktor :** framework Kotlin qui est ici utilisé pour faire une API web JSON ([cf. site officiel](https://ktor.io/))
  - **Draw.io :** dessin de diagrammes en ligne
  - **BitBucket :** forge logiciel (dépôt de code source, bugtracker ...)
  - **Git :** gestionnaire de version de code source décentralisé

## Planification

Pour réaliser l'activité je me suis organisé de la manière suivante :  

  - analyse et conception global
  - codage ... Pour chaque module/classe :  
    - implémentation
    - test (manuel)
    - documentation du code source ([KDoc](https://kotlinlang.org/docs/reference/kotlin-doc.html))
  - test global (manuel)

## Analyse et conception global

Diagramme de classe du projet :  

![Diagramme de classe du projet](classDiagramOCWebServer.png)  

## Réalisation

TODO  

## Conclusion

TODO  
